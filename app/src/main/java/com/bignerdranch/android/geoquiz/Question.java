package com.bignerdranch.android.geoquiz;

public class Question {
    private int mTextResId;
    private boolean mAnswertTrue;

    private boolean mhasCheated;

    public Question(int mTextResId, boolean mAnswertTrue) {
        this.mTextResId = mTextResId;
        this.mAnswertTrue = mAnswertTrue;
    }

    public int getmTextResId() {
        return mTextResId;
    }

    public void setmTextResId(int mTextResId) {
        this.mTextResId = mTextResId;
    }

    public boolean ismAnswertTrue() {
        return mAnswertTrue;
    }

    public void setmAnswertTrue(boolean mAnswertTrue) {
        this.mAnswertTrue = mAnswertTrue;
    }

    public boolean isMhasCheated() {
        return mhasCheated;
    }

    public void setMhasCheated(boolean mhasCheated) {
        this.mhasCheated = mhasCheated;
    }
}
